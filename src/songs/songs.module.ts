import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SongsController } from './songs.controller';
import { Song, SongSchema } from './song.schema';
import { SongsService } from './songs.service'

@Module({
    imports: [MongooseModule.forFeature([{ name: Song.name, schema: SongSchema }])],
    controllers: [SongsController],
    providers: [SongsService],
    exports: [SongsService]
})
export class SongsModule { }