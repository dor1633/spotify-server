import { Injectable } from '@nestjs/common';
import { Song } from './song.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class SongsService {
  constructor(@InjectModel(Song.name) private readonly songsModel: Model<Song>) {}

  async findAll(): Promise<Song[]> {
    return this.songsModel.find();
  }

  async getSongsByArtistId(artistId : string) : Promise<Song[]> {
    return this.songsModel.find({artistsIds: artistId});
  }
}