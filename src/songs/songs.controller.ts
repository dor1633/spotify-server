import { Controller, Get, Param } from '@nestjs/common';
import { SongsService } from './songs.service';
import { Song } from '../dtos/song.dto'
import { ApiResponse, ApiOkResponse , ApiNotFoundResponse, ApiTags } from '@nestjs/swagger';

@ApiTags("Songs")
@Controller("Songs")
export class SongsController {
  constructor(private songsService: SongsService) { }

  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'The found song',
    type: Song,
  })
  async getSongById(@Param('id') id: string): Promise<Song[]> {
    return this.songsService.findAll();
  }
}
