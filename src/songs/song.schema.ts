import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Song extends Document {
  @Prop()
  artistsIds: [string];

  @Prop()
  songName: string;

  @Prop()
  songFileName: string;
}

export const SongSchema = SchemaFactory.createForClass(Song);