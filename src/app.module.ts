import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import config from './config/config.json'
import { ArtistsModule } from './artists/artists.moudle';
import { SongsModule } from './songs/songs.module';

@Module({
  imports: [MongooseModule.forRoot(`${config.mongoAddress}/${config.dataBaseName}`), ArtistsModule, SongsModule],
})

export class AppModule {}
