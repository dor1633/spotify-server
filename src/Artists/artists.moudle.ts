import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArtistsController } from './artists.controller';
import { Artist, ArtistSchema } from './artist.schema';
import { ArtistsService } from './artists.service'
import { SongsModule } from 'src/songs/songs.module';

@Module({
    imports: [MongooseModule.forFeature([{ name: Artist.name, schema: ArtistSchema }]), SongsModule],
    controllers: [ArtistsController],
    providers: [ArtistsService]
})
export class ArtistsModule { }