import { Controller, Get, Param, Query } from '@nestjs/common';
import { ArtistsService } from './artists.service';
import { Artist } from '../dtos/artist.dto'
import { ApiResponse, ApiOkResponse, ApiNotFoundResponse, ApiTags } from '@nestjs/swagger';
import { SongsService } from 'src/songs/songs.service';
import { Song } from 'src/songs/song.schema';

@Controller("Artists")
@ApiTags("Artists")
export class ArtistsController {
  constructor(private artistsService: ArtistsService,
    private songsService: SongsService) { }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'The found artists',
    type: [Artist],
  })
  async getAllArtists(): Promise<Artist[]> {
    return this.artistsService.findAll();
  }

  @Get("/search")
  @ApiResponse({
    status: 200,
    description: 'The artists that contains string',
    type: [Artist],
  })
  async getArtistsThatContainsString(@Query('expression') expression: string): Promise<Artist[]> {

    return this.artistsService.findByExistingExpression(expression);
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'The found artist',
    type: Artist,
  })
  async getArtistById(@Param('id') id: string): Promise<Artist | null> {
    return this.artistsService.findOne(id);
  }

  @Get(':artistId/songs')
  @ApiResponse({
    status: 200,
    description: 'Get the songs of the artist',
    type: Artist,
  })
  async getSongsOfArtist(@Param('artistId') artistId: string): Promise<Song[]> {
    return this.songsService.getSongsByArtistId(artistId);
  }
}
