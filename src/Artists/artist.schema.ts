import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Artist extends Document {
  @Prop()
  hebrewName: string;

  @Prop()
  englishName: string;

  @Prop()
  pictureUrl: string;
}

export const ArtistSchema = SchemaFactory.createForClass(Artist);