import { Injectable } from '@nestjs/common';
import { Artist } from './artist.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class ArtistsService {
  constructor(@InjectModel(Artist.name) private readonly artistsModel: Model<Artist>) { }

  async findAll(): Promise<Artist[]> {
    return await this.artistsModel.find();
  }

  async findByExistingExpression(expression: string): Promise<Artist[]> {
    return await this.artistsModel.find({hebrewName: {$regex: `.*${expression}.*`}});
  }

  async findOne(id: string): Promise<Artist | null> {
    return await this.artistsModel.findOne({ _id: id });
  }
}
