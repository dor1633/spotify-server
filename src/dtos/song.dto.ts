import { ApiProperty } from '@nestjs/swagger';

export class Song {
  @ApiProperty()
  artistsIds: [string];

  @ApiProperty()
  songName: string;

  @ApiProperty()
  songFileName: string
}