import { ApiProperty } from '@nestjs/swagger';

export class Artist {
  @ApiProperty()
  hebrewName: string;

  @ApiProperty()
  englishName: string;

  @ApiProperty()
  pictureUrl: string
}